from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from pymongo import MongoClient
from typing import List
from bson import ObjectId, json_util
import json
import uvicorn

app = FastAPI()


# Connexion à la base de données en MongoDB
client = MongoClient("mongodb://mongodb:27017/") 
db = client["carpart"]
collection = db["produits"]

class Product(BaseModel):
    name: str
    description: str
    quantity: int

class CustomJsonEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)


@app.get("/getProduct/{product_id}")
def get_product(product_id: str):
    product = collection.find_one({"_id": ObjectId(product_id)})
    if not product:
        raise HTTPException(status_code=404, detail="Product not available")
    else:
        return json.loads(json.dumps(product, cls = CustomJsonEncoder))


@app.put("/updateProduct/{product_id}")
def update_product(product_id: str, product: Product):
    result = collection.update_one({"_id": ObjectId(product_id)}, {"$set": product.dict()})
    if result.modified_count == 0:
        raise HTTPException(status_code=404, detail="Product not available")
    return {"message": "Product successfully updated"}


@app.post("/addProduct")
def add_product(product: Product):
    result = collection.insert_one(product.dict())
    return {"id": str(result.inserted_id)}


@app.delete("/delete_product/{product_id}")
def delete_product(product_id: str):
    result = collection.delete_one({"_id": ObjectId(product_id)})
    if result.deleted_count == 0:
        raise HTTPException(status_code=404, detail="Product not available")
    return {"message": "Product successfully deleted"}



if __name__=="__main__":
    uvicorn.run(app, host="0.0.0.0", port=3000)
