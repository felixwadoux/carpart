import time
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from sqlalchemy import create_engine, Column, Integer, String, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from decouple import config  

# Configuration et connexion à la base de données MySQL
DATABASE_URL = config("DATABASE_URL")
print(DATABASE_URL)

time.sleep(10)

engine = create_engine(DATABASE_URL)
metadata = MetaData()

Base = declarative_base(metadata=metadata)
class Client(Base):
    __tablename__ = "clients"

    id = Column(Integer, primary_key=True, index=True)
    nom = Column(String(50), index=True)
    prenom = Column(String(50))  
    email = Column(String(100), unique=True, index=True) 
    nb_commandes = Column(Integer)


# Vérifie si la table existe, sinon la crée
Base.metadata.create_all(bind=engine, checkfirst=True)
metadata.create_all(engine)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

app = FastAPI()

class ClientCreate(BaseModel):
    nom: str
    prenom: str
    email: str
    nb_commandes: int

@app.post("/add_client")
def add_client(client: ClientCreate):
    db_client = Client(**client.dict())
    db = SessionLocal()
    db.add(db_client)
    db.commit()
    db.refresh(db_client)
    db.close()
    return {"message": "Client added successfully", "client_id": db_client.id}

@app.put("/update_client/{client_id}")
def update_client(client_id: int, client_update: ClientCreate):
    db = SessionLocal()
    db_client = db.query(Client).filter(Client.id == client_id).first()
    if db_client:
        for key, value in client_update.dict().items():
            setattr(db_client, key, value)
        db.commit()
        db.refresh(db_client)
        db.close()
        return {"message": "Client updated successfully", "new_values": client_update.dict()}
    else:
        db.close()
        raise HTTPException(status_code=404, detail="Client not found")

@app.delete("/delete_client/{client_id}")
def delete_client(client_id: int):
    db = SessionLocal()
    db_client = db.query(Client).filter(Client.id == client_id).first()
    if db_client:
        db.delete(db_client)
        db.commit()
        db.close()
        return {"message": "Client deleted successfully"}
    else:
        db.close()
        raise HTTPException(status_code=404, detail="Client not found")

@app.get("/get_client/{client_id}")
def get_client(client_id: int):
    db = SessionLocal()
    db_client = db.query(Client).filter(Client.id == client_id).first()
    if db_client:
        db.close()
        return {"nom": db_client.nom, "prenom": db_client.prenom, "email": db_client.email, "nb_commandes": db_client.nb_commandes}
    else:
        db.close()
        raise HTTPException(status_code=404, detail="Client not found")